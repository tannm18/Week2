import random
import numpy as np
from matplotlib import pyplot as plt

# N = input("Enter an integer from 1 to 10: ")
# N = int(N)

N = 20

int_list = []
for x in range(N):
    int_list.append(random.randint(1, 10))

print(int_list)

# bins = np.linspace(1, 10, 20)
# plt.hist(int_list, bins=bins, alpha=0.5)
# plt.xlim(1, 10)
# plt.title('Histogram of random data in range [1 10]')
# plt.xlabel('x')
# plt.ylabel('Count')
# plt.show()

print('Maximum value of this list: ', max(int_list))
print('Minimum value of this list: ', min(int_list))
print('Variance of this list: ', np.var(int_list))
print('Standard deviation of this list: ', np.std(int_list))

distinct_items = list(set(int_list))
print('List after removing duplicate items:', distinct_items)