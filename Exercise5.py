import numpy as np

a = np.matrix('1 2 3 4 5; 3 4 2 5 6; 1 6 3 2 5')
print('Sum of all elements of matrix:', np.sum(a))