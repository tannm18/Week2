from datetime import  datetime

date1 = input("Enter date 1 (dd/mm/YYYY): ")
date1_object = datetime.strptime(date1, '%d/%m/%Y')
print('Date 1: ', date1_object)

date2 = input("Enter date 2 (dd/mm/YYYY): ")
date2_object = datetime.strptime(date2, '%d/%m/%Y')
print('Date 2: ', date2_object)

date_diff = date2_object - date1_object
print('Number of date among', date1_object, 'and', date2_object, 'is', date_diff)
